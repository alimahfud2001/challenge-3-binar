package com.example.challenge3

import android.os.Bundle
import android.text.Layout
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.challenge3.databinding.FragmentFirstBinding
import com.example.challenge3.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.button.setOnClickListener{
            if (binding.et.text.isNullOrEmpty()){
                Toast.makeText(requireContext(), "Harap Diisi", Toast.LENGTH_SHORT).show() }
            else {
                val actionToThirdFragment = SecondFragmentDirections.actionSecondFragmentToThirdFragment(null)
                actionToThirdFragment.nama = binding.et.text.toString()
                it.findNavController().navigate(actionToThirdFragment)
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}