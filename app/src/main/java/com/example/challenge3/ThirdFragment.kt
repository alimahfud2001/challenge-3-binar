package com.example.challenge3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.challenge3.databinding.FragmentThirdBinding
import kotlin.properties.Delegates


class ThirdFragment : Fragment() {
    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val arg_nama = ThirdFragmentArgs.fromBundle(arguments as Bundle).nama

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Parcelable>("identity")?.observe(viewLifecycleOwner){ result ->
            binding.tv2.text = "${result.usia} Tahun, ${checking(result.usia!!)}"
            binding.tv2.visibility = View.VISIBLE
            binding.tv3.text = result.alamat
            binding.tv3.visibility = View.VISIBLE
            binding.tv4.text = result.pekerjaan
            binding.tv4.visibility = View.VISIBLE
        }

        if (arg_nama != null){
            binding.tv1.visibility = View.VISIBLE
            binding.tv1.text = arg_nama
        }

        binding.button.setOnClickListener{
            it.findNavController().navigate(R.id.action_thirdFragment_to_fourthFragment)
        }
    }

    private fun checking(input: Int): String{
        if (input!!%2 == 0) return "Bernilai Genap"
        else return "Bernilai Ganjil"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}