package com.example.challenge3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.challenge3.databinding.FragmentFourthBinding


class FourthFragment : Fragment() {
    private var _binding: FragmentFourthBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFourthBinding.inflate(inflater, container, false)
        return binding.root}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            if (binding.et.text.isNullOrEmpty()||binding.et2.text.isNullOrEmpty()||binding.et3.text.isNullOrEmpty()){
                Toast.makeText(requireContext(), "Harap Diisi", Toast.LENGTH_SHORT).show()
            }
            else{
                println(binding.et.text.toString().toInt())
                val data = Parcelable(binding.et.text.toString().toInt(), binding.et2.text.toString(), binding.et3.text.toString())
                findNavController().previousBackStackEntry?.savedStateHandle?.set("identity", data)
                findNavController().popBackStack()
            }
        }
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}